import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/**
 * Generated class for the CookbookFooterComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'cookbook-footer',
  templateUrl: 'cookbook-footer.html'
})
export class CookbookFooterComponent {

  constructor(public navCtrl: NavController) {}

  gotoPage(page){
    this.navCtrl.push(page);
  }

}
