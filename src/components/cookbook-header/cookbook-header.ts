import { Component } from '@angular/core';

/**
 * Class for the CookbookHeaderComponent component.
 */
@Component({
  selector: 'cookbook-header',
  templateUrl: 'cookbook-header.html'
})
export class CookbookHeaderComponent {

  constructor() {
  }

}
