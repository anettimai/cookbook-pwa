import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CookbookFooterComponent } from './cookbook-footer/cookbook-footer';
import { UserDataComponent } from './user-data/user-data';
import { CookbookHeaderComponent } from './cookbook-header/cookbook-header';
@NgModule({
	declarations: [
    CookbookFooterComponent,
    UserDataComponent,
    CookbookHeaderComponent],
	imports: [IonicModule],
	exports: [
    CookbookFooterComponent,
    UserDataComponent,
    CookbookHeaderComponent,]
})
export class ComponentsModule {}
