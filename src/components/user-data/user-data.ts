import { Events } from 'ionic-angular';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';

/**
 * UserDataComponent component.
 */
@Component({
  selector: 'user-data',
  templateUrl: 'user-data.html'
})
export class UserDataComponent {

  user;

  constructor(public authService: AuthServiceProvider, public events: Events) {
    // Set the current user.
    this.getUser();
  }

  /**
   * 
   */
  ionViewDidLoad() {
    this.getUser();
  }

  /**
   * Navigate to specific page.
   * @param  {string} page
   */
  gotoPage(page) {
    this.events.publish('app:goto', page);
  }

  /**
   * Log the user out.
   */
  logout() {
    this.authService.logout();
  }

  getUser() {
    this.authService.getUser().then((user) => {
      this.user = user;
    });
  }

}
