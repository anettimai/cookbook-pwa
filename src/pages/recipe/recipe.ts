import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';
import { environment } from '../../environments/environment';

/**
 * Class for the RecipePage page.
 */

@IonicPage({
  name: 'RecipePage',
  segment: 'recipe/:recipeId'
})
@Component({
  selector: 'page-recipe',
  templateUrl: 'recipe.html',
})
export class RecipePage {

  recipe;
  recipeId: string;
  imgUrl: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthServiceProvider,
    public restService: RestServiceProvider) {
      this.recipeId = this.navParams.get('recipeId');
      this.getRecipe();

      this.imgUrl = environment.base_url + '/img/recipes/';
      if (environment.production) {
        this.imgUrl = '';
      }
  }

  /**
   * Check if user is logged in, 
   * only let authenticated users access the page.
   */
  ionViewCanEnter(): boolean | Promise<any> {
    return this.authService.isAuthenticated(this.navCtrl);
  }

  ionViewDidLoad() {
  }

  /**
   * Get the recipe.
   */
  getRecipe() {
    this.restService.getData('', 'recipes/' + this.recipeId, 'get').subscribe((data) => {
      if (data) {
        this.recipe = data.recipe;
      }
    });
  }

}
