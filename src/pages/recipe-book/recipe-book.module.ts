import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecipeBookPage } from './recipe-book';

@NgModule({
  declarations: [
    RecipeBookPage,
  ],
  imports: [
    IonicPageModule.forChild(RecipeBookPage),
    ComponentsModule
  ],
})
export class RecipeBookPageModule {}
