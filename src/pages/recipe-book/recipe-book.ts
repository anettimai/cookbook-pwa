import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, ToastController, AlertController } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';
import { environment } from '../../environments/environment';
import { SharePage } from './../share/share';

/**
 * Class for the RecipeBookPage page.
 */

@IonicPage({
  name: 'RecipeBookPage',
  segment: 'recipe-book/:id'
})
@Component({
  selector: 'page-recipe-book',
  templateUrl: 'recipe-book.html',
})
export class RecipeBookPage {

  recipes: any;
  recipeBookTitle: string;
  id: string;
  imgUrl;
  loader: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthServiceProvider,
    public restService: RestServiceProvider,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController) {
    this.presentLoading();
    this.id = this.navParams.get('id');
    this.imgUrl = environment.base_url + '/img/recipes/';
    this.getRecipes();

    if (environment.production) {
      this.imgUrl = '';
    }
  }

  /**
   * Check if user is logged in, 
   * only let authenticated users access the page.
   */
  ionViewCanEnter(): boolean | Promise<any> {
    return this.authService.isAuthenticated(this.navCtrl);
  }

  /**
   * Navigate to page.
   * @param  {string} page
   */
  goToPage(page) {
    this.navCtrl.push(page, {
      book_id: this.id
    });
  }
  
  /**
   * Get the recipes in the book.
   */
  getRecipes() {
    this.restService.getData('', 'recipe-books/' + this.id + '/recipes', 'get').subscribe((data) => {
      if (data) {
        if (data.recipes && data.recipes !== undefined) {
          this.recipeBookTitle = data.book_title;
          this.recipes = data.recipes;
          this.loader.dismiss();
        }
      }
    });
  }

  presentLoading() {
    this.loader = this.loadingCtrl.create({
      content: "Loading..."
    });
 
    this.loader.present();
  }

  /**
   * Redirect to the recipe page.
   */
  openRecipe(recipeId) {
    this.navCtrl.push('RecipePage', {
      'recipeId': recipeId
    });
  }

  presentModal() {
    const modal = this.modalCtrl.create(SharePage, {bookId: this.id});
    modal.present();
  }

  deleteRecipe() {
    this.restService.getData(this.id, 'recipe-books/', 'delete').subscribe((data) => {
      if (data.success) {
        this.presentToast('Recipe book has been successfully deleted!');
        this.navCtrl.push('HomePage');
      }
    });
  }

  showDeleteConfirm() {
    let alert = this.alertCtrl.create({
        title: 'Confirm delete recipe book.',
        message: 'Are you sure you want to permanently delete this recipe book?',
        buttons: [
            {
                text: 'No',
                handler: () => {}
            },
            {
                text: 'Yes',
                handler: () => {
                   this.deleteRecipe();
                }
            }
        ]
    });

    alert.present();
  }

  /**
   * Show notification message.
   * @param  {string} msg
   */
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
