import { RegexValidators } from './../../app/validators';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  userData = { email: '', password: '' };
  errorMessage = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public authService: AuthServiceProvider,
    public restService: RestServiceProvider,
    private toastCtrl:ToastController,
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController) {
      this.menuCtrl.enable(false);
      this.loginForm = this.formBuilder.group({
        email: ['', Validators.compose([
          Validators.pattern(RegexValidators.email),
          Validators.required
        ])],
        password: ['', Validators.compose([
          //Validators.pattern(RegexValidators.password),
          Validators.required
        ])]
      });
  }

  ionViewDidLoad() {}

  /**
   * Check if user is logged in, 
   * only let non-authenticated users access the page.
   */
  ionViewCanEnter(): boolean | Promise<any> {
    return this.authService.isNotAuthenticated(this.navCtrl);
  }

  /**
   * Handle login functionality.
   */
  login() {
    // Check if the form passed the validation.
    if (this.loginForm.valid) {
      this.getFormData(this.loginForm.controls);
      this.restService.getData(this.userData, 'login', 'post').subscribe((data) => {
        // If login is successful, redirect the user to the Homepage. 
        if (data.success) {
          this.authService.login(data);
          this.navCtrl.push('HomePage');
          this.presentToast("Successful login.");
        }
        // If the login fails due to wrong credentials,
        // display error message.
        else if (data.status == 'fail') {
          this.errorMessage = "The login details are incorrect.";
          this.presentToast(this.errorMessage);
        }
        // If the login fails due to other reasons,
        // display error message.
        else {
          this.presentToast("Something went wrong.");
        }
      });
    }
    else {
      this.presentToast("All fields are required.");
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  getFormData(data) {
    return this.userData = {
      email: data.email.value,
      password: data.password.value
    }
  }

}
