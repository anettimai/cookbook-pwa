import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, reorderArray, ToastController } from 'ionic-angular';
import { parse } from 'recipe-ingredient-parser';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';

/**
 * Generated class for the AddRecipePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-recipe',
  templateUrl: 'add-recipe.html',
})
export class AddRecipePage {

  id;
  recipeForm: FormGroup;
  base64textString;
  data = { 
    title: '',
    notes: '',
    servings: '',
    prep_time: '',
    cooking_time: '',
    photo: '',
    ingredients: [],
    steps: [],
    book_id: ''
  };
  errorMessage = '';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public restProvider: RestServiceProvider,
    public authService: AuthServiceProvider,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder) {
      this.id = this.navParams.data.book_id;

      // If the ID is not present, redirect to the homepage.
      if (!this.id) {
        this.navCtrl.push('HomePage');
        this.presentToast("Something went wrong.");
      }

      this.recipeForm = this.formBuilder.group({
        title: ['', Validators.compose([
          Validators.required
        ])],
        notes: ['', Validators.compose([
          Validators.required
        ])],
        prep_time: ['', Validators.compose([
          Validators.required
        ])],
        cooking_time: ['', Validators.compose([
          Validators.required
        ])],
        servings: ['', Validators.compose([
          Validators.required
        ])],
        ingredient: [''],
        step: [''],
        file: ['']
      });
  }

  /**
   * Check if user is logged in, 
   * only let authenticated users access the page.
   */
  ionViewCanEnter(): boolean | Promise<any> {
    return this.authService.isAuthenticated(this.navCtrl);
  }

  ionViewDidLoad() {}

  addRecipe() {
    // Check if the ID is set and the form is valid.
    if (this.recipeForm.valid && this.id) {
      this.getFormData(this.recipeForm.controls);
      this.restProvider.getData(this.data, 'recipes', 'post').subscribe((data) => {
        if (data.success) {
          this.navCtrl.push('RecipeBookPage', { id: this.id });
          this.presentToast("Successfully added a new recipe!");
        }
        else {
          this.presentToast("Something went wrong.");
        }
      });
    }
    else {
      if (this.id) {
        this.errorMessage = "All fields are required.";
        this.presentToast("All fields are required.");
      } else {
        this.navCtrl.push('HomePage');
        this.presentToast("Something went wrong.");
      }
      
    }
  }

  /**
   * Add ingredient to the ingredients array.
   */
  addIngredient() {
    let formData = this.recipeForm.controls;
    let ingredient;
    
    if (formData.ingredient.value) {
      // Parse the string and save the array to the data object.
      ingredient = parse(formData.ingredient.value);
      this.data.ingredients.push(ingredient);

      // Clear the ingredient input.
      this.recipeForm.controls.ingredient.reset();
    }
  }

  /**
   * Add step to the steps array.
   */
  addStep() {
    let formData = this.recipeForm.controls;

    if (formData.step.value) {
      this.data.steps.push(formData.step.value);
      this.recipeForm.controls.step.reset();
    }
  }

  removeIngredient(ingredient) {
    var index = this.data.ingredients.indexOf(ingredient, 0);
    if (index > -1) {
      this.data.ingredients.splice(index, 1);
    }
  }

  removeStep(step) {
    var index = this.data.steps.indexOf(step, 0);
    if (index > -1) {
      this.data.steps.splice(index, 1);
    }
  }

  reorderItems(indexes){
    this.data.steps = reorderArray(this.data.steps, indexes);
  }

  /**
   * Show notification message.
   * @param  {string} msg
   */
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  /**
   * Generate base64 from the image file.
   * @param readerEvent
   */
  handleReaderLoaded(readerEvent) {
    var binaryString = readerEvent.target.result;
    this.base64textString = btoa(binaryString);
  }

    /**
   * Create array for the post request.
   * @param  {array} formData
   */
  getFormData(formData) {
    return this.data = {
      title: formData.title.value,
      notes: formData.notes.value,
      prep_time: formData.prep_time.value,
      cooking_time: formData.cooking_time.value,
      servings: formData.servings.value,
      photo: this.base64textString,
      ingredients: this.data.ingredients,
      steps: this.data.steps,
      book_id: this.id
    }
  }

  /**
   * Get file input data.
   * @param event
   */
  handleFileSelect(event){
    let files = event.target.files;
    let file = files[0];

    if (files && file) {
        let reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }
}
