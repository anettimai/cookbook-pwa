import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRecipeBookPage } from './add-recipe-book';

@NgModule({
  declarations: [
    AddRecipeBookPage,
  ],
  imports: [
    IonicPageModule.forChild(AddRecipeBookPage),
    ComponentsModule
  ],
})
export class AddRecipeBookPageModule {}
