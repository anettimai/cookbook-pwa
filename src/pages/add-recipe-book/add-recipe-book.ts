import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { FormBuilder, Validators } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';

/**
 * Class for the AddRecipeBookPage page.
 */
@IonicPage({
  name: 'AddRecipeBookPage',
  segment: 'recipe-book/new'
})
@Component({
  selector: 'page-add-recipe-book',
  templateUrl: 'add-recipe-book.html',
})
export class AddRecipeBookPage {
  recipeBookForm;
  data = { title: '', description: '', photo: '', user: ''};
  base64textString;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProvider: RestServiceProvider,
    public authService: AuthServiceProvider,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder) {
      this.recipeBookForm = this.formBuilder.group({
        title: ['', Validators.compose([
          Validators.required
        ])],
        description: [''],
        file: ['']
      });
  }

  ionViewDidLoad() {}

  /**
   * Check if user is logged in, 
   * only let authenticated users access the page.
   */
  ionViewCanEnter(): boolean | Promise<any> {
    return this.authService.isAuthenticated(this.navCtrl);
  }

  /**
   * Save a new recipe book.
   */
  addRecipeBook() {
    this.getFormData(this.recipeBookForm.controls);
    this.restProvider.getData(this.data, 'recipe-books', 'post').subscribe((data) => {
      if (data.success) {
        this.navCtrl.push('HomePage');
        this.presentToast("Successfully created a new recipe book!");
      }
      else {
        this.presentToast("Something went wrong.");
      }
    });
  }

  /**
   * Get file input data.
   * @param event
   */
  handleFileSelect(event){
    let files = event.target.files;
    let file = files[0];

    if (files && file) {
        let reader = new FileReader();
        reader.onload = this.handleReaderLoaded.bind(this);
        reader.readAsBinaryString(file);
    }
  }

  /**
   * Generate base64 from the image file.
   * @param readerEvent
   */
  handleReaderLoaded(readerEvent) {
    var binaryString = readerEvent.target.result;
    this.base64textString = btoa(binaryString);
  }

  /**
   * Create array for the post request.
   * @param  {array} formData
   */
  getFormData(formData) {
    return this.data = {
      title: formData.title.value,
      description: formData.description.value,
      photo: this.base64textString,
      user: this.authService.getUserId()
    }
  }

  /**
   * Show notification message.
   * @param  {string} msg
   */
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}
