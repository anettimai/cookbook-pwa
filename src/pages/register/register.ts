import { RegexValidators } from './../../app/validators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, MenuController } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';

/**
 * Class for the RegisterPage page.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  userData = { name: '', email: '', password: '' };
  registrationForm: FormGroup;
  errorMessage = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authService: AuthServiceProvider,
    public restService: RestServiceProvider,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController) {
      this.menuCtrl.enable(false);
      this.registrationForm = this.formBuilder.group({
        name: ['', Validators.compose([
          Validators.required
        ])],
        email: ['', Validators.compose([
          Validators.pattern(RegexValidators.email),
          Validators.required
        ])],
        password: ['', Validators.compose([
          Validators.pattern(RegexValidators.password),
          Validators.required
        ])]
      });
  }

  ionViewDidLoad() {}

  /**
   * Check if user is logged in, 
   * only let non-authenticated users access the page.
   */
  ionViewCanEnter(): boolean | Promise<any> {
    return this.authService.isNotAuthenticated(this.navCtrl);
  }

  register() {
    if (this.registrationForm.valid) {
      this.getFormData(this.registrationForm.controls);
      this.restService.getData(this.userData, 'register', 'post').subscribe((data) => {
        if (data.success) {
          this.authService.login(data);
          this.navCtrl.push('HomePage');
          this.presentToast("Successful registration.");
        }
        else {
          this.presentToast("Something went wrong.");
        }
      });
    }
    else {
      this.presentToast("All fields are required.");
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  getFormData(data) {
    return this.userData = {
      name: data.name.value,
      email: data.email.value,
      password: data.password.value
    }
  }

}
