import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { NavController, IonicPage, MenuController } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';
import { environment } from '../../environments/environment';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public authService: AuthServiceProvider,
    public restService: RestServiceProvider,
    public menuCtrl: MenuController) {
      this.menuCtrl.enable(true);
      this.imgUrl = environment.base_url + '/img/recipe-books/';
      this.getRecipeBooks();

      if (environment.production) {
        this.imgUrl = '';
      }
  }

  recipeBooks = [];
  imgUrl = '';
  searchQuery: string = '';

  /**
   * Check if user is logged in, 
   * only let authenticated users access the page.
   */
  ionViewCanEnter(): boolean | Promise<any> {
    return this.authService.isAuthenticated(this.navCtrl);
  }

  /**
   * Navigate to page.
   * @param  {string} page
   */
  goToPage(page) {
    this.navCtrl.push(page);
  }

  /**
   * Get the logged in user's recipe books.
   */
  getRecipeBooks() {
    let user_id = this.authService.getUserId();
    this.restService.getData('', 'users/recipe-book/' + user_id, 'get').subscribe((data) => {
      if (data) {
        this.recipeBooks = data.recipeBooks;
      }
    });
  }

  openRecipeBook(id) {
    this.navCtrl.push('RecipeBookPage', {
      'id': id
    });
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.getRecipeBooks();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    setTimeout(() => {
      if (val && val.trim() != '') {
        //this.recipeBooks = this.recipeBooks.filter(item => item.title.includes(val));
        this.recipeBooks = this.recipeBooks.filter((item) => {
          return (item.title.toLowerCase().includes(val));
        })
      }
    }, 1000)
  }

}
