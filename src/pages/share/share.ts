import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';

/**
 * Class for the SharePage page.
 */

@IonicPage()
@Component({
  selector: 'page-share',
  templateUrl: 'share.html',
})
export class SharePage {

  book_id;
  users;
  email: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restService: RestServiceProvider,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController) {
      this.book_id = this.navParams.get('bookId');
      this.getUsers();
    }

  ionViewDidLoad() {}

  /**
   * Get the recipe book.
   */
  getUsers() {
    this.restService.getData('', 'recipe-books/' + this.book_id + '/users', 'get').subscribe((data) => {
      if (data) {
        this.users = data.users;
      }
    });
  }

  /**
   * Handle Recipe Book sharing.
   */
  share() {
    let user_email = { email: this.email };
    this.restService.getData(user_email, 'recipe-books/' + this.book_id + '/share', 'post').subscribe((data) => {
      if (data.message) {
        this.presentToast(data.message);
      }
      this.viewCtrl.dismiss();
    });
  }

  /**
   * Show notification message.
   * @param  {string} msg
   */
  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }
}