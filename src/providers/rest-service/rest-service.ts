import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHeaderResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

/*
  Class for the RestProvider provider.
*/
@Injectable()
export class RestServiceProvider {

  apiUrl = environment.api_url;

  constructor(public http: HttpClient) {
  }

  /**
   * Handle the post requests.
   * @param {array} payload
   * @param {string} type
   */
  getData(payload, type, requestType) {
    let body = payload;
    let authToken = '';

    if (type !== 'login' && type !== 'register') {
      authToken = localStorage.getItem('token');
    }

    let fcm = localStorage.getItem('fcm');

    if (fcm == null) {
      fcm = '';
    }

    let headers = new HttpHeaders({
      'api-token': environment.api_key,
      'user-key': authToken,
      'fcm-key': fcm,
      'Accept': 'application/json'
    });
    let options = new HttpHeaderResponse({ headers:headers });

    if (requestType == 'post') {
      return this.http.post(this.apiUrl + '/' + type, body, options)
        .map(res => res)
        .catch(this.handleError);
    } 
    else if (requestType == 'get') {
      return this.http.get(this.apiUrl + '/' + type + payload, options)
        .map(res => res)
        .catch(this.handleError);
    }
    else if (requestType == 'delete') {
      return this.http.delete(this.apiUrl+ '/' + type + payload, options)
      .catch(this.handleError);
    }
  }

  /**
   * @param  {} error
   */
  handleError(error) {
    return Observable.throw(error || 'Server error');
  }

}
