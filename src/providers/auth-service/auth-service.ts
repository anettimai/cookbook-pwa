import { Events, NavController } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { JwtHelper, AuthHttp, tokenNotExpired } from 'angular2-jwt';
import { Storage } from "@ionic/storage";
import { HttpClient } from '@angular/common/http';

// Import RxJS components.
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthServiceProvider {

  apiUrl = environment.api_url;
  jwtHelper: JwtHelper = new JwtHelper();
  userId = '';

  constructor(
    public http: HttpClient,
    private storage: Storage,
    public authHttp: AuthHttp,
    public events: Events) {}

  /**
   * Save the user's Jwt token in the local storage after login.
   * @param  {array} payload
   * @returns void
   */
  login(payload): void {
    localStorage.setItem('token', payload.token);
    this.storage.set('token', payload.token);
    this.storage.set('user', payload.user);
  }
  
  /**
   * Check if user is logged in.
   */
  isLoggedIn() {
    return tokenNotExpired();
  }

  isAuthenticated(nav: NavController): boolean | Promise<any> {
    if (!this.isLoggedIn()) {
      setTimeout(() => {
        nav.setRoot('WelcomePage');
      }, 0);
    }
    return this.isLoggedIn();
  }

  isNotAuthenticated(nav: NavController): boolean | Promise<any> {
    if (this.isLoggedIn()) {
      setTimeout(() => {
        nav.setRoot('HomePage');
      }, 0);
    }
    return true;
  }

  /**
   * Remove the user's token from the local storage.
   */
  logout(): void {
    localStorage.removeItem('token');
    this.storage.remove('token');
    this.storage.remove('user');
    this.events.publish('user:logout');
  };

  getUser(): Promise<any> {
    return this.storage.get('user').then((value) => {
      return value;
    });
  }

  getUserId() {
    let token = localStorage.getItem('token');
    if (token) {
      let decodedToken = this.jwtHelper.decodeToken(token);
      return decodedToken['sub'];
    }
    return null;
  }
}