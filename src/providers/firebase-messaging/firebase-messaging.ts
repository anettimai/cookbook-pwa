import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import 'firebase/messaging';
import { RestServiceProvider } from '../rest-service/rest-service';

@Injectable()
export class FirebaseMessagingProvider {
  private messaging;
  private unsubscribeOnTokenRefresh = () => { };

  constructor(
    public restService: RestServiceProvider
  ) {
    if (this.firebaseIsSupported()) { 
      this.messaging = firebase.messaging();
      navigator.serviceWorker.register('service-worker.js').then((registration) => {
        this.messaging.useServiceWorker(registration);
        this.disableNotifications();
        this.enableNotifications();
      });
    }
  }

  public firebaseIsSupported() {
    return firebase.messaging.isSupported();
  }

  public enableNotifications() {
    // Requesting permission.
    return this.messaging.requestPermission().then(() => {
      // token might change - we need to listen for changes to it and update it
      this.setupOnTokenRefresh();
      this.listen();
      let token = this.updateToken();
      return token;
    }).catch(function(err) {
      // Unable to get permission to notify.
    });
  }

  public disableNotifications() {
    this.unsubscribeOnTokenRefresh();
    this.unsubscribeOnTokenRefresh = () => { };
    //return this.storage.set('fcmToken', '').then();
  }

  public updateToken() {
    return this.messaging.getToken().then((currentToken) => {
      if (currentToken) {
        localStorage.setItem('fcm', currentToken);
        return currentToken;
      } else {
        // No Instance ID token available. Request permission to generate one.
      }
    });
  }

  private setupOnTokenRefresh(): void {
    this.unsubscribeOnTokenRefresh = this.messaging.onTokenRefresh(() => {
      // Token refreshed
      localStorage.set('fcm', '').then(() => { this.updateToken(); });
    });
  }

  /**
   * Get the current FCM push token.
   * @return p Promise returning the push token
   */
  getPushToken(): Promise<string> {
    return this.messaging.getToken();
  }

  listen() {
    this.messaging.onMessage((payload) => {
    });
  }

}