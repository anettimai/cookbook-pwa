import { FirebaseMessagingProvider } from './../providers/firebase-messaging/firebase-messaging';
import { AuthServiceProvider } from './../providers/auth-service/auth-service';
import { Component, ViewChild } from '@angular/core';
import { Events } from 'ionic-angular';
import { LoadingController, Nav, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  public isLoggedIn;
  rootPage: any;
  loader: any;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Home', component: 'HomePage' },
  ]

  constructor(
    public menu: MenuController,
    public loadingCtrl: LoadingController,
    public storage: Storage,
    private authService: AuthServiceProvider,
    public events: Events,
    private firebaseService: FirebaseMessagingProvider
  ){
    this.presentLoading();
    this.menu.enable(true);
    this.isLoggedIn = this.authService.isLoggedIn();
      
    // Check if the user is logged in.
    if (this.authService.isLoggedIn()) {
      this.rootPage = 'HomePage';
    } else {
      this.rootPage = 'WelcomePage';
    }

    if (firebaseService.firebaseIsSupported()) {
      this.firebaseService.enableNotifications();
    }

    this.listenToAppEvents();
    this.loader.dismiss();
  }

  presentLoading() {
 
    this.loader = this.loadingCtrl.create({
      content: "Loading..."
    });
 
    this.loader.present();
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  listenToAppEvents() {
    this.listenToLoginEvents();

    this.events.subscribe('app:goto', (page) => {
      this.menu.close();
      this.nav.setRoot(page);
    });
  }

  listenToLoginEvents() {
    this.events.subscribe('user:logout', () => {
      this.menu.close();
      this.nav.setRoot('WelcomePage');
    });
  }
}

