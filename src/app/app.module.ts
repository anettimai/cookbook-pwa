import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { Http, RequestOptions, HttpModule } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { AngularFireModule, FirebaseApp } from '@angular/fire';
import { firebaseConfig } from '../environments/firebaseconfig';

// Pages
import { MyApp } from './app.component';

// Providers
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { RestServiceProvider } from '../providers/rest-service/rest-service';
import { FirebaseMessagingProvider } from '../providers/firebase-messaging/firebase-messaging';

// Components
import { ComponentsModule } from './../components/components.module';

export function getAuthHttp(http, storage: Storage) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true,
    tokenGetter: (() => {
      return storage.get('token').then(token => {
        return token;
      });
    }),
  }), http);
}

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ComponentsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp, {
      preloadModules: true
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    AuthServiceProvider,
    {
      provide: ErrorHandler, 
      useClass: IonicErrorHandler
    },
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http, RequestOptions]
    },
    RestServiceProvider,
    FirebaseMessagingProvider
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
  constructor(firebaseWebApp: FirebaseApp) {}
}
